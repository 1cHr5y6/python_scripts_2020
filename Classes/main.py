# How to create classes and methods in separate modules
from modules import *

animal1 = animals()
animal1_color = animal1.get_color()
animal1_legs = animal1.get_legs()

print(animal1_color)
print(animal1_legs)
