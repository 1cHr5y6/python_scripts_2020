#modules.py

class animals:
    type = "land"
    color = "green"
    legs = 4

    def get_color(self):
        return "This animal is the color " + self.color + "."

    def get_legs(self):
	# To avoid concatentation errors from str + int concatenation
	return "This animal has {} legs.".format(self.legs) 
	


