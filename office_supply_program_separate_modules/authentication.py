import stdiomask

# Function if user authentication fails
def access_denied():
    print("Access denied.")
    print("")

# Function for third user authentication attempt
def final_auth():
    name3 = input("Enter username: ")
    auth3 = (stdiomask.getpass(mask='*'))
    if(name3 == "Bob" and auth3 == "123"):
        print("Hello, Bob.")
        print("Access granted.")
    elif(name3 == "Alice" and auth3 == "456"):
        print("Hello, Alice.")
        print("Access granted.")
    elif(name3 == "Jane" and auth3 == "789"):
        print("Hello, Jane.")
        print("Access granted.")
    else:
        access_denied()
        print("Exiting program...")
    

# Function for second user authentication attempt
def second_auth():
    # If you are having trouble with debugging, uncomment the following line below
    # to troubleshoot:
    # print("DEBUG: second_auth")
    name2 = input("Enter username: ")
    auth2 = (stdiomask.getpass(mask='*'))

    if (name2 == "Bob" and auth2 == "123"):
        print("")
        print("Hello, Bob.")
        print("Access granted.")
    elif (name2 == "Alice" and auth2 == "456"):
        print("")
        print("Hello, Alice.")
        print("Access granted.")
    elif (name2 == "Jane" and auth2 == "789"):
        print("")
        print("Hello, Jane.")
        print("Access granted.")
    else:
        access_denied()
        final_auth()