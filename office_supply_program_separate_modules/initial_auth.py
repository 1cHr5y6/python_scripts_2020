import stdiomask
from authentication import access_denied, second_auth 

# Function for initial user authentication

# Ask user for username and password
def initial_auth():
    print("")
    print("")
    name = input("Enter username: ")
    auth = (stdiomask.getpass(mask='*'))

# Authenticate user
    if(name == "Bob" and auth == "123"):
        print("")
        print("Hello, Bob.")
        print("Access granted.")
    elif(name == "Alice" and auth == "456"):
        print("")
        print("Hello, Alice.")
        print("Access granted.")
    elif(name == "Jane" and auth == "789"):
        print("")
        print("Hello, Jane.")
        print("Access granted.")
    else:
        print("Invalid entry.")
        access_denied()
        second_auth()
