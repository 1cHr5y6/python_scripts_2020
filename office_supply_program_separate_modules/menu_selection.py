import sys
from datetime import date
import re

# Global definitions
today = date.today()
d1 = today.strftime("%m/%d/%Y")

# Menu selection
def menu_selection():
    while True:
        try:
            menu_selection = int(input("Enter menu selection: "))
            print("")
        except ValueError:
            print("This is not a valid entry.")
            continue
        else:
            break

# =====================  S E L E C T I O N   1 ================================
    if menu_selection == 1:
        paper_quantity = (int(input("How many boxes of copy paper would you like to order? ")))

        paper = float(17.99)
        total_price = (float(paper * paper_quantity))
        print_price = '${:,.2f}'.format(float(total_price))

        print("The total amount is ", print_price)
        department_number = input("Enter the department number: ")

        print("--------------  O R D E R   C O N F I R M A T I O N  -----------------") 
        print("")
        print("On ", d1, " you ordered {0} box(es) of copy paper for {1}.".format(paper_quantity, print_price))
        print("Department ", department_number, " will be invoiced for this purchase.")
        print("")
        print("End of program.")

# =====================  S E L E C T I O N   2 ================================
    if menu_selection == 2:
        pen_quantity = int(input("How many boxes of black pens would you like to order? "))
        black_pens = float(14.99)
        total_price1 = (float(black_pens * pen_quantity))
        pen_price = '${:,.2f}'.format(float(total_price1))

        print("The total amount is ", pen_price)
        department_number1 = input("Enter your department number: ")
        print("")

        print("--------------  O R D E R   C O N F I R M A T I O N  -----------------") 
        print("")
        print("On ", d1, " you ordered {0} box(es) of black pens for {1}.".format(pen_quantity, pen_price))
        print("Department ", department_number1, " will be invoiced for this purchase.")
        print("")
        print("End of program.")

# =====================  S E L E C T I O N   3 ================================
    if menu_selection == 3:
        legalPads_quantity = int(input("how many 10 count packs of legal pads would you like to order? "))
        legalPads = float(12.99)
        total_price2 = float(legalPads_quantity * legalPads)
        legalPads_price = '${:,.2f}'.format(float(total_price2))

        print("The total amount is ", legalPads_price)
        department_number2 = input("Enter the department number: ")
        print("")

        print("--------------  O R D E R   C O N F I R M A T I O N  -----------------") 
        print("")
        print("On ", d1, " you ordered {0} pack(s) of legal pads for {1}.".format(legalPads_quantity, legalPads_price))
        print("Department ", department_number2, " will be invoiced for this purchase.")
        print("")
        print("End of program.")

# =====================  S E L E C T I O N   4 ================================
    elif menu_selection == 4:
        # Use the import sys with sys.exit to exit program
        # sys.exit("Exiting program...\n" + "\n")
        print("Exiting program...")

    else:
        print("Exiting program...")
        #sys.exit("Exiting program...\n" + "\n")
    





    
