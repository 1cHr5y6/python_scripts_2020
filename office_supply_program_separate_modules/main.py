import sys
from datetime import date
import re
import stdiomask
from datetime import date
from getpass import getpass
from initial_auth import initial_auth
from main_header import main_header
from main_menu_header import main_menu_header
from menu_selection import menu_selection


# Global definitions
#today = date.today()
#d1 = today.strftime("%m/%d/%Y")

# Initial authentication
initial_auth()

# Main header 
main_header()

# Main Menu header
main_menu_header()

# Menu selection:
menu_selection()

