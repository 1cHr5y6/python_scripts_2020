import re
from datetime import date
from os import system, name
from time import sleep
import stdiomask

# Global declarations
today = date.today()
d1 = today.strftime("%m" + "-" + "%d" + "-" + "%Y")


print("")
name = raw_input("Enter your name: ")
# Ask user to enter account number
auth=(stdiomask.getpass(prompt = "Enter your account number: "))

# Pause before clearing screen
def clear():
    # for windows
    if name == 'nt':
        _ = system('cls')

    # for mac and linux
    else:
        _ = system('clear')

if ((name == 'Bob') or (name == "bob") and (auth == '3800938')) :
    print("")
    sleep(2)
    clear()

    print("=======================================")
    print("")
    print("             Company ABC               ") 
    print("=======================================")
    print "Today's date: ", d1

    print("")
    print "Hello, {0}.".format(name)
    print("")
    print("You have logged in to your account.")
    print("")
    print("")
else:
    print("")
    print("Invalid account number.")
    print("Access denied.")
    print("")
    print("Exiting program...")


    # Credits:
    # https://www.geeksforgeeks.org/clear-screen-python
    # https://www.pypi.org/project/stdiomask
