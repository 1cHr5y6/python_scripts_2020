from datetime import date
import stdiomask
import sys
import re

# Global definitions
today = date.today()
d1 = today.strftime("%m/%d/%Y")

# Function to ask user to enter department number
def deptNum():
    deptNum = raw_input("Enter your department number:\n")

# Function if user authentication fails 
def access_denied():
    print("Access denied.")
    print(" ")

# Function for third user authentication attempt 
def final_auth():
    name3=raw_input("Enter username: ")
    auth3=(stdiomask.getpass(prompt='PW: ')) 

    if ( name3=="Bob" and  auth3 == "123" ):
        print("Access granted.")
        print("Hello, Bob.")
    elif ( name3 == "Alice" and auth3 == "456"):
        print("Access granted.")
        print("Hello, Alice.")
    elif (name3 == "Jane" and auth3 == "789"):
        print("Access granted.")
        print("Hello, Jane.")
    else:
        access_denied()
        print("Exiting program.")


# Function for second user authentication attempt
def authentication():
    # If having trouble with code uncomment the following line below
    # to troubleshoot
    #print( "DEBUG: second auth" )
    name2=raw_input("Enter username: ")
    auth2=(stdiomask.getpass(prompt='PW: ')) 

    if ( name2 == "Bob" and auth2 == "123" ):
        print("")
        print("Access granted." )
        print("Hello, Bob." )
    elif ( name2 == "Alice" and auth2 == "456" ):
        print("")
        print("Access granted." )
        print("Hello, Alice." )
    elif ( name2 == "Jane" and auth2 == "789" ):
        print("")
        print( "Access granted." )
        print( "Hello, Jane." )
    else:
        access_denied()
        final_auth()

# Ask for user name and passphrase
print(" ")
print(" ")
name=raw_input("Enter username: ")
auth=(stdiomask.getpass(prompt='PW: '))

# User authentication
if ( name == "Bob" and auth == "123" ):
    print(" ")
    print("Hello, Bob.")
    print("Access granted.")

elif ( name == "Alice" and auth == "456" ):
    print(" ")
    print("Hello, Alice")
    print("Access granted.")
elif ( name == "Jane" and auth == "789" ):
    print(" ")
    print("Hello, Jane.")
    print("Access granted.")
else:
    print("Invalid entry.")
    access_denied()
    authentication()


# Main Header
print ""
print ""
print'________________________ Office Menu Program_____________________'
print ""
print ""

# Menu header
print "                         M A I N  *** M E N U        "
print ""
print("1. 5 count box of copy paper")
print("2. 20 count box of black pens")
print("3. 10 count pack of legal pads")
print("4. Exit program")
print ""
menuSelection = 0
while True:
    try:
        menuSelection = int(raw_input("Enter menu selection: "))
        print""
    except ValueError: 
        print("This is not a valid entry.")
        continue
    else:
        break

# ================== S E L E C T I O N   1 ======================
if menuSelection == 1:
    paper_quantity = (int(raw_input("How many boxes of copy paper would you like to order? ")))

    paper = float(17.99)
    totalPrice0 = (float(paper * paper_quantity))
    print_price = '${:,.2f}'.format(float(totalPrice0))

    print "The total amount is ",  print_price
    print ""
    print "Your department will be invoiced for this purchase."
    print ""

    deptNum = raw_input("Enter your department number:\n")
    print ""

    print (color.BOLD + color.CYAN + "=================== O R D E R     C O N F I R M A T I O N ===============" + color.END)
    print ""
    print "On", d1, "you ordered {0} box(es) of copy paper for {1}".format(paper_quantity, print_price)
    print ""
    print "Department", deptNum, "Will be billed for this purchase."
    print ""
    print "End of program."
    print ""
    print ""

# ================== S E L E C T I O N   2 ======================
elif menuSelection == 2:
    pen_quantity = (int(raw_input("How many 20 count boxes of black pens would you like to order? ")))
    black_pens = float(14.99)
    totalPrice1 = (float(black_pens * pen_quantity))
    pen_price = '${:,.2f}'.format(float(totalPrice1))

    print "The total amount is ", pen_price
    print ""
    print "Your department  will be invoiced for this purchase."
    print ""

    deptNum = raw_input("Enter your department number:\n")
    print ""

    print (color.BOLD + color.CYAN + "=================== O R D E R     C O N F I R M A T I O N ===============" + color.END)
    print ""
    print "On", d1, "you ordered {0} box(es) of copy paper for {1}".format(pen_quantity, pen_price)
    print ""
    print "Department", deptNum, "Will be billed for this purchase."
    print ""
    print "End of program."
    print ""
    print ""



# ================== S E L E C T I O N   3 ======================
elif menuSelection == 3:
    legalPads_quantity = (int(raw_input("How many 10 count packs of legal pads would you like to order? ")))
    legalPads_price = float(12.99)
    totalPrice3 = (float(legalPads_quantity *  legalPads_price))
    legalPads_price = '${:,.2f}'.format(float(totalPrice3))

    print "The total amount is: ", legalPads_price
    print ""
    print "Your department will be invoiced for this purchase."
    print ""

    deptNum = raw_input("Enter your department number:\n")
    print ""

    print "=================== O R D E R     C O N F I R M A T I O N ==============="
    print ""
    print "On", d1, "you ordered {0} box(es) of copy paper for {1}".format(legalPads_quantity, legalPads_price)
    print ""
    print "Department", deptNum, "Will be billed for this purchase."
    print ""
    print "End of program."
    print ""
    print ""



# ================== S E L E C T I O N   4 ======================
elif menuSelection == 4:
    # Use the import sys with sys.exit to exit program
    sys.exit( "Exiting program...\n" + "\n" )
    
else:
    sys.exit( "Exiting program...\n" + "\n" )













