# dice_roll_simulator.py
import random
from Banners import main_banner, selection_banner
from termcolor import colored


# Header for program
main_banner()    

# Header for selection menu
selection_banner()

for j in range(0,3):
    print('')
    print(colored(''' 1. roll the dice             2. exit     ''', "cyan"))
    print('')

    user = int(input(colored("What would you like to do\n", "cyan")))
    if user==1:
        number = random.randint(1,6)
        for i in range(0,3):
            user = int(input(colored("Guess the number: ", "cyan")))
            if user == number:
                print('')
                print(colored("* * * N i c e  o n e , m a t e !! * * * ", "cyan"))
                print('')
                print(colored(("You guessed the number! It's " + str(number)), "cyan"))
                print('')
                quit()

        if user != number:
            print('')
            print(colored("Your guess is incorrect! The number is: " + str(number)))
    else:
       quit() 

