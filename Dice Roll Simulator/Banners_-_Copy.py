# banners.py
from termcolor import colored
from time import sleep
from clear import clear


def main_banner():
    print colored('', "cyan")
    print colored('', "cyan")
    print colored('======================================================', "cyan")
    print colored('====                                              ====', "cyan")
    print colored('====             Dice Roll Game                   ====', "cyan")
    print colored('====                                              ====', "cyan")
    print colored('======================================================', "cyan")
    print colored('', "cyan")
    sleep(3)
    clear()


def selection_banner():
    print colored('-------------------------------------------------', "cyan")                                              
    print colored('----                                         ----', "cyan")                                              
    print colored('----            Make a Selection             ----', "cyan")                                              
    print colored('----                                         ----', "cyan")                                              
    print colored('-------------------------------------------------', "cyan")                                              
    print colored('', "cyan")                                                             
